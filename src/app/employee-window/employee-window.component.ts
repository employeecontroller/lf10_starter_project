import {Component, OnInit} from '@angular/core';
import {EmployeeServiceService} from "../employee-service.service";
import {Employee, areEqual} from "../Employee";
import {employeeSortFunctions} from "../EmployeeSortFunctions";

@Component({
  selector: 'app-employee-window',
  templateUrl: './employee-window.component.html',
  styleUrls: ['./employee-window.component.css']
})
export class EmployeeWindowComponent implements OnInit {
  sortFunctions: { name: string, fn: any }[];
  searchedId: number = NaN;

  persistentEmployees: Employee[] = [];
  viewingEmployees: Employee[] = [];

  openedEmployee: Employee = new Employee();
  editingEmployee: Employee = new Employee();

  constructor(public __employeeService: EmployeeServiceService) {
    this.sortFunctions = employeeSortFunctions;
  }

  public refreshList(): void {
    this.__employeeService.fetchData().then((response) => {
      if (response) {
        this.persistentEmployees = response;
        this.showAll();
        this.sortEmployees();
        console.log('Successfully refreshed')
      }
    }).catch((e) => this.printErrorMessage(e));
  }

  public printErrorMessage(e: string): void {
    console.error(e)
    alert('Es ist ein Fehler aufgetreten!');
  }

  public handleSearchChange(): void {
    if (this.searchedId === 0 || isNaN(this.searchedId)) {
      this.showAll();
      return;
    }
    this.viewingEmployees = this.persistentEmployees.filter(employee => employee.id === this.searchedId);
  }

  public showAll(): void {
    this.viewingEmployees = this.persistentEmployees;
    this.searchedId = NaN;
  }

  public sortEmployees(): void {
    const comboBox: HTMLSelectElement | null = <HTMLSelectElement>document.getElementById('sortComboBox');
    if (!comboBox) return;
    const selection: number = comboBox.options[comboBox.selectedIndex].index;
    if (selection >= 0 && selection < this.sortFunctions.length) {
      this.viewingEmployees.sort(this.sortFunctions[selection].fn);
    }
  }


  public toggleEmployeeTab(employee: Employee): void {
    if (this.employeeTabIsOpened(employee)) {
      this.openedEmployee = new Employee();
      this.editingEmployee = new Employee();
    } else {
      this.openedEmployee = employee;
      this.editingEmployee = Object.assign(this.editingEmployee, employee);
    }
  }

  public employeeTabIsOpened(employee: Employee): boolean {
    return (this.openedEmployee != null && areEqual(this.openedEmployee, employee));
  }

  public changesWereMade(): boolean {
    return !areEqual(this.editingEmployee, this.openedEmployee);
  }

  public saveChanges(): void {
    if (this.editingEmployee) {
      this.__employeeService.update(this.editingEmployee.id, this.editingEmployee).then(() => {
        this.refreshList();
        console.log('Successfully updated')
        alert('Mitarbeiter erfolgreich bearbeitet!');
      }).catch((e) => this.printErrorMessage(e));
    }
  }

  public discardChanges(): void {
    this.editingEmployee = Object.assign(this.editingEmployee, this.openedEmployee);
    this.viewingEmployees = this.persistentEmployees;
  }


  public deleteEmployee(): void {
    if (this.openedEmployee) {
      if (confirm('Wollen sie den Mitarbeiter löschen?')) {
        this.__employeeService.delete(this.openedEmployee.id).then(() => {
          this.refreshList();
          console.log('Successfully deleted');
          alert('Mitarbeiter erfolgreich gelöscht!');
        }).catch((e) => this.printErrorMessage(e));
      }
    }
  }

  ngOnInit() {
    this.refreshList();
  }
}
