import {Employee} from "./Employee";

export const employeeSortFunctions: { name: string, fn: any }[] = [
  {
    name: 'Vorname A-Z',
    fn: function firstnameAToZ(employeeA: Employee, employeeB: Employee) {
      if (employeeA.firstName! > employeeB.firstName!) {
        return 1
      }
      if (employeeA.firstName! < employeeB.firstName!) {
        return -1
      }
      return 0;
    }
  },
  {
    name: 'Vorname Z-A',
    fn: function firstnameZToA(employeeA: Employee, employeeB: Employee) {
      if (employeeA.firstName! < employeeB.firstName!) {
        return 1
      }
      if (employeeA.firstName! > employeeB.firstName!) {
        return -1
      }
      return 0;
    }
  },
  {
    name: 'Nachname A-Z',
    fn: function lastnameAToZ(employeeA: Employee, employeeB: Employee) {
      if (employeeA.lastName! > employeeB.lastName!) {
        return 1
      }
      if (employeeA.lastName! < employeeB.lastName!) {
        return -1
      }
      return 0;
    }
  },
  {
    name: 'Nachname Z-A',
    fn: function lastnameZToA(employeeA: Employee, employeeB: Employee) {
      if (employeeA.lastName! < employeeB.lastName!) {
        return 1
      }
      if (employeeA.lastName! > employeeB.lastName!) {
        return -1
      }
      return 0;
    }
  }


]
