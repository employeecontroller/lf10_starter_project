import {Component, OnInit} from '@angular/core';
import {Employee} from "../Employee";
import {EmployeeServiceService} from "../employee-service.service";

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  firstName: string = '';
  lastName: string = '';
  street: string = '';
  postcode: string = '';
  city: string = '';
  phone: string = '';

  constructor(private __employeeService: EmployeeServiceService) {
  }

  ngOnInit(): void {
  }

  public postData(): void {
    let employee = new Employee(0, this.firstName, this.lastName, this.street, this.postcode, this.city, this.phone);
    this.__employeeService.create(employee).then(() => {
      console.log('success');
      alert('Mitarbeiter wurde erfolgreich hinzugefügt');
    })
      .catch((e) => {
        CreateEmployeeComponent.handleErrorPromise(e);
        alert('Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingaben.');
      });
  }

  private static handleErrorPromise(error: Response | any): Promise<never> {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

}
