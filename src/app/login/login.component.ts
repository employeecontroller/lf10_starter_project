import {Component, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EmployeeServiceService} from "../employee-service.service";

interface ServerResponse {
  access_token: string,
  expires_in: number,
  refresh_expires_in: number,
  refresh_token: string,
  token_type: string,
  not_before_policy: number,
  session_state: string,
  scope: string,
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: string = ''; // should be user
  password: string = ''; // should be test

  private sperreTimestamp: number | undefined = undefined;
  private currentTime: number = 0;
  private count: number = 0;

  // private service = new EmployeeServiceService(this.http);

  constructor(private fb: FormBuilder, private route: Router, private http: HttpClient, public __employeeService: EmployeeServiceService) {
  }

  ngOnInit(): void {
  }

  public login(): void {
    this.currentTime = Date.now() / 1000;
    if (this.sperreTimestamp === undefined || this.currentTime - this.sperreTimestamp > 120) {
      if (this.count < 3) {
        this.sperreTimestamp = undefined;
        this.callBearerToken();
      } else {
        this.sperreTimestamp = Date.now() / 1000;
        this.count = 0;
        alert('Sie hatten zu viele Fehlversuche und werden deshalb für 2 Minuten gesperrt.');
      }
    } else {
      this.route.navigate([''])
        .then(() => {
          alert('Sie sind noch gesperrt.');
        });
    }
  }

  public callBearerToken(): void {
    let body = "grant_type=password&client_id=employee-management-service&username=" +
      this.user + "&password=" + this.password;

    this.http.post<ServerResponse>('/bearer',
      body, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }).toPromise()
      .then((response) => {
        if (response !== undefined) {
          this.count = 0;
          window.localStorage.setItem('bearer', response.access_token);
          this.route.navigate(['/home'])
            .then(() => {
              console.log('success', this.currentTime);
            });
          return;
        } else {
          this.countUpRerouteAndAlertWrongCredentials();
        }
      })
      .catch((e) => {
        console.log('Fehler', e);
        this.countUpRerouteAndAlertWrongCredentials();
      })
  }

  public countUpRerouteAndAlertWrongCredentials(): void {
    this.count++;
    this.route.navigate([''])
      .then(() => {
        alert('Login falsch.');
        this.password = '';
      });
  }
}
