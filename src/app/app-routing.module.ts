import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import {CreateEmployeeComponent} from "./create-employee/create-employee.component";
import {LoginComponent} from "./login/login.component";
import {EmployeeWindowComponent} from "./employee-window/employee-window.component";

const routes: Routes = [
  { path: 'home', component: HomepageComponent },
  { path: 'create', component: CreateEmployeeComponent },
  { path: '', component: LoginComponent},
  { path: 'search', component: EmployeeWindowComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
