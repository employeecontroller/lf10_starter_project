import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Employee} from "./Employee";

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  constructor(private http: HttpClient) {}

  public getBearerToken(): string {
    return <string>window.localStorage.getItem('bearer');
  }

  public fetchData(): Promise<Employee[] | undefined> {
     return this.http.get<Employee[]>('/backend', {
       headers: new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', `Bearer ${this.getBearerToken()}`)
     }).toPromise();
  }

  public create(employee: Employee): Promise<Employee | undefined> {
    return this.http.post<Employee>('/backend', employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.getBearerToken()}`)
    }).toPromise();
  }

  public read(id: number | undefined): Promise<Employee | undefined> {
    return this.http.get<Employee>('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.getBearerToken()}`)
    }).toPromise();
  }

  public update(id: number | undefined, employee: Employee): Promise<Employee | undefined> {
    return this.http.put<Employee>('/backend/' + id, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.getBearerToken()}`)
    }).toPromise();
  }

  public delete(id: number | undefined): Promise<Employee | undefined> {
    return this.http.delete('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.getBearerToken()}`)
    }).toPromise();
  }

  ngOnInit() {}
}
