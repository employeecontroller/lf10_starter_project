export class Employee {
  constructor(public id?: number,
              public lastName?: string,
              public firstName?: string,
              public street?: string,
              public postcode?: string,
              public city?: string,
              public phone?: string) {
  }
}

export function areEqual(employee1: Employee, employee2: Employee) {
  return employee1.id === employee2.id &&
    employee1.lastName === employee2.lastName &&
    employee1.firstName === employee2.firstName &&
    employee1.street === employee2.street &&
    employee1.postcode === employee2.postcode &&
    employee1.city === employee2.city &&
    employee1.phone === employee2.phone;
}
